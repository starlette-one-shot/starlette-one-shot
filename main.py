from starlette.applications import Starlette
from starlette.routing import Route
from starlette.requests import Request
from starlette.responses import PlainTextResponse, JSONResponse
from starlette.templating import Jinja2Templates


templates = Jinja2Templates(directory="templates")

# creates end points
# takes in a request


async def index(request: Request):
    student_id = request.path_params.get("student_id")
    return PlainTextResponse(content=f"Hello {student_id}")


async def json_endpoint(request: Request):
    return JSONResponse(content={"message": "Hello World"})


async def html_endpoint(request: Request):
    context = {"request": request}
    return templates.TemplateResponse("index.html", context)

routes = [
    Route("/{student_id:int}/", endpoint=index),
    Route("/json", endpoint=json_endpoint),
    Route('/html', endpoint=html_endpoint)
]

app = Starlette(
    debug=True,
    routes=routes
)
